import { Http, Response, Headers } from '@angular/http';
import { AuthenticationService } from './../auth/authentification.service';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Job } from './model/job';
import 'rxjs/add/operator/map';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';

/**
 *     Service for OAR API Access
 *     __More info about the API :__ http://oar.imag.fr/docs/latest/user/api.html
 *     You can change the protocole and API entry point in the enrivonment config file.
 *
 */
@Injectable()
export class OarApiService {
    urlMedia: string;
    apiRoot = 'http://localhost:46668/oarapi';

    constructor(private http: HttpClient) { }

    /**
       *     Get all current jobs
       *     __return format :__ JSON
       */
    getJobs(): any {
        // const headers = this.generateHeaders();
        const url = this.apiRoot + '/jobs.json';
        return;
    }


    /**
       * Return a JSON of given job by id
       */
    getJobsById(ids: number[]): any {
        // const headers = this.generateHeaders();
        const url = this.apiRoot + '/jobs.json';
        return;
    }

    getJob(id: string): any {
        // const headers = this.generateHeaders();
        const url = this.apiRoot + '/jobs.json';
        return;
    }
}
