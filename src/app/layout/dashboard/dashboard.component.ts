import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    public sliders: Array<any> = [];

    constructor() {
        this.sliders.push(
            // Images displayed on this page are not claimed to be created by or owned by us.
            // All right reserved to OAR's site creator.
            {
                imagePath: 'assets/images/banner1.jpg',
                label: '',
                text: ''
            },
            {
                imagePath: 'assets/images/banner2.jpg',
                label: '',
                text: ''
            },
            {
                imagePath: 'assets/images/banner3.jpg',
                label: '',
                text: ''
            }
        );
    }

    ngOnInit() {}
}
