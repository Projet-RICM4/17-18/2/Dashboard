import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResourcesRoutingModule } from './resources-routing.module';
import { ResourcesComponent } from './resources.component';
import { PageHeaderModule } from './../../shared';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        ResourcesRoutingModule,
        PageHeaderModule,
        HttpClientModule
    ],
    declarations: [ResourcesComponent]
})
export class ResourcesModule { }
