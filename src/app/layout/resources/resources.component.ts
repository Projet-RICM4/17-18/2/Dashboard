import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

export interface ApiResult {
    offset: number;
    api_timestamp: number;
    items: RssResult;
    links: any;
    total: number;
}

export interface RssResult {
    id: number;
    state: string;
    available_upto: number;
    network_address: string;
}

@Component({
    selector: 'app-resources',
    templateUrl: './resources.component.html',
    styleUrls: ['./resources.component.scss'],
    animations: [routerTransition()]
})


export class ResourcesComponent implements OnInit {
    apiRoot = 'http://localhost:46668/oarapi';
    values: ApiResult;
    defined: boolean;
    items: RssResult;

    constructor(private router: Router, private http: HttpClient) {
        this.showRessources();
    }

    ngOnInit() {
    }

    getRessources() {
        this.defined = true;
        const url = this.apiRoot + '/resources.json';
        // const headers = this.generateHeaders();
        // this.values = this.http.get<ApiResult[]>(url);
        return this.http.get<ApiResult>(url);

    }

    showRessources() {
        this.getRessources()
            .subscribe(data => {
                this.values = {
                    offset: data['offset'],
                    api_timestamp: data['api_timestamp'],
                    items: data['items'],
                    links: data['links'],
                    total: data['total']
                };
                this.items = this.values.items;
                console.log(this.values);
            });
    }

    gotoAddRss() {
        this.router.navigate(['/formrss']);
    }

}
