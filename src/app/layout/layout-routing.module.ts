import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'resources', loadChildren: './resources/resources.module#ResourcesModule' },
            { path: 'formrss', loadChildren: './formrss/formrss.module#FormRssModule' },
            { path: 'formjob', loadChildren: './formjob/formjob.module#FormJobModule' },
            { path: 'jobs', loadChildren: './jobs/jobs.module#JobsModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
