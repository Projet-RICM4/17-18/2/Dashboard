import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormJobComponent } from './formjob.component';

const routes: Routes = [
    {
        path: '', component: FormJobComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FormJobRoutingModule {
}
