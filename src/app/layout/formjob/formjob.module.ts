import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FormJobRoutingModule } from './formjob-routing.module';
import { FormJobComponent } from './formjob.component';
import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [CommonModule, FormJobRoutingModule, PageHeaderModule, FormsModule],
    declarations: [FormJobComponent]
})
export class FormJobModule {}
