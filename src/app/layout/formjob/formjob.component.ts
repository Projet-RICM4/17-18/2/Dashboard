import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

export class Job {
    nodes: number;
    name: string;
    properties: string;
    cpu: number;
    script: string;
    types: string;
    walltime: string;
    directory: string;
    reservationDates: string;

    constructor() { }
}

@Component({
    selector: 'app-formjob',
    templateUrl: './formjob.component.html',
    styleUrls: ['./formjob.component.scss'],
    animations: [routerTransition()]
})
export class FormJobComponent {
    jobsForm = new Job();
    submitted = false;

    constructor() { }

    onSubmit() {
        this.submitted = true;
        console.log('Submitted');
    }

}
