import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormJobComponent } from './formjob.component';

describe('FormJobComponent', () => {
    let component: FormJobComponent;
    let fixture: ComponentFixture<FormJobComponent>;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                declarations: [FormJobComponent]
            }).compileComponents();
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(FormJobComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
