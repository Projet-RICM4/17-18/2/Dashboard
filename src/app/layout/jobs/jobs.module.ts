import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobsRoutingModule } from './jobs-routing.module';
import { JobsComponent } from './jobs.component';
import { PageHeaderModule } from './../../shared';
import { HttpClientModule, HttpClient } from '@angular/common/http';
 
@NgModule({
  imports: [
    CommonModule,
    JobsRoutingModule,
    PageHeaderModule,
    HttpClientModule 
  ],
  declarations: [JobsComponent]
})
export class JobsModule { }
