import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { OarApiService } from '../../shared/services/oar-api/oar-api.service';
import { environment } from './../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/observable/of';

export interface ApiResult {
    offset: number;
    api_timestamp: number;
    items: JobsResult;
    links: any;
    total: number;
}

export interface JobsResult {
    id: number;
    state: string;
    available_upto: number;
    network_address: string;
    owner: string;
    submission: number;
    api_timestamp: number;
    queue: string;
    name: string;
    links: any;
}
@Component({
    selector: 'app-jobs',
    templateUrl: './jobs.component.html',
    styleUrls: ['./jobs.component.scss'],
    animations: [routerTransition()],
    providers: [OarApiService]
})
export class JobsComponent implements OnInit {
    apiRoot = 'http://localhost:46668/oarapi';
    values: ApiResult;
    defined: boolean;
    items: JobsResult;


    constructor(private http: HttpClient, private router: Router) {
        this.defined = false;
        this.showJobs();
    }

    getJobs() {
        this.defined = true;
        const url = this.apiRoot + '/jobs.json';
        // const headers = this.generateHeaders();
        // this.values = this.http.get<ApiResult[]>(url);
        return this.http.get<ApiResult>(url);
    }

    showJobs() {
        this.getJobs()
            .subscribe(data => {
                this.values = {
                    offset: data['offset'],
                    api_timestamp: data['api_timestamp'],
                    items: data['items'],
                    links: data['links'],
                    total: data['total']
                };
                this.items = this.values.items;
                console.log(this.values);
            });
    }

    ngOnInit() {
    }

    gotoAddJob() {
        this.router.navigate(['/formjob']);
    }

}
