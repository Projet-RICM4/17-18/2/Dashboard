import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormRssComponent } from './formrss.component';

describe('FormRssComponent', () => {
    let component: FormRssComponent;
    let fixture: ComponentFixture<FormRssComponent>;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                declarations: [FormRssComponent]
            }).compileComponents();
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(FormRssComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
