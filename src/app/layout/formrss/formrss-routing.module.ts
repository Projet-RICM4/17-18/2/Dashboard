import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormRssComponent } from './formrss.component';

const routes: Routes = [
    {
        path: '', component: FormRssComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FormRssRoutingModule {
}
