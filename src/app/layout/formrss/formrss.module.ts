import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormRssRoutingModule } from './formrss-routing.module';
import { FormRssComponent } from './formrss.component';
import { PageHeaderModule } from './../../shared';

@NgModule({
    imports: [CommonModule, FormRssRoutingModule, PageHeaderModule],
    declarations: [FormRssComponent]
})
export class FormRssModule { }
