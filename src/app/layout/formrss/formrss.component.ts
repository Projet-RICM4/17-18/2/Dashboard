import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

@Component({
    selector: 'app-form',
    templateUrl: './formrss.component.html',
    styleUrls: ['./formrss.component.scss'],
    animations: [routerTransition()]
})
export class FormRssComponent implements OnInit {
    submitted = false;

    constructor() { }

    ngOnInit() { }

    onSubmit() {
        this.submitted = true;
        console.log('Submitted');
    }
}
