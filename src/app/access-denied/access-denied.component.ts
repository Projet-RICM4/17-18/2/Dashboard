import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { Router } from '@angular/router';

@Component({
    selector: 'app-access-denied',
    templateUrl: './access-denied.component.html',
    styleUrls: ['./access-denied.component.scss'],
    animations: [routerTransition()]
})
export class AccessDeniedComponent implements OnInit {

    constructor(private router: Router) { }

    ngOnInit() {
    }

    goBack() {
        this.router.navigate(['/dashboard']);
    }
}
