import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { NgForm } from '@angular/forms';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    constructor(public router: Router) { }
    login = '';
    ngOnInit() { }

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }
    onSubmit(f: NgForm) {
        if (f.value.nameLogin = 'root') {
            this.router.navigate(['/dashboard']);
            this.login = 'root';
        }
        console.log(f.valid);
    }

}
